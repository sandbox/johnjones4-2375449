(function($,Drupal) {
  Drupal.behaviors.menu_pages = {
    divs: {},
    scrollTo: function(uri) {
      var $div = this.divs[uri];
      if ($div) {
        $('html,body').animate({
          scrollTop: $div.offset().top
        });
      }
    },
    normalizeLink: function(link) {
      var bases = [
        window.location.origin + Drupal.settings.basePath,
        Drupal.settings.basePath
      ];
      for(var i=0; i<bases.length; i++) {
        if (link.indexOf(bases[i]) == 0) {
          return link.substring(bases[i].length);
        }
      }
      return link;
    },
    attach: function(context, settings) {
      var that = this;

      // Build URI list
      that.divs = {};
      $('.menu-pages-segment').each(function() {
        var $div = $(this);
        var uri = $div.attr('data-uri');
        if (uri) {
          that.divs[uri] = $div;
        }
      });

      // Handle hash changes
      $(window).on('hashchange',function(event) {
        try { event.preventDefault() } catch(e) {}
        that.scrollTo(window.location.hash.substring(1));
        return false;
      });

      // Setup content links
      $('a').each(function() {
        var $a = $(this);
        var href = $a.attr('href');
        if (href) {
          href = that.normalizeLink(href);
          if (that.divs[href]) {
            $a.addClass('in-page-link');
          }
        }
      });
      $('a.in-page-link').click(function(event) {
        try { event.preventDefault() } catch(e) {}
        var href = that.normalizeLink($(this).attr('href'));
        window.location.hash = '#' + href;
        return false;
      });

      // Handle hash set on page load
      var timer = setTimeout(function() {
        if (window.location.hash) that.scrollTo(window.location.hash.substring(1));
      },3000);
      $(window).load(function() {
        clearTimeout(timer);
        if (window.location.hash) that.scrollTo(window.location.hash.substring(1));
      });
    }
  }
})(jQuery,Drupal);